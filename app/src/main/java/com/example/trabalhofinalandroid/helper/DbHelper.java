package com.example.trabalhofinalandroid.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

    public static  int VERSION = 2;
    public static String NOME_DB ="DB_ALUNOS";
    public static String TABELA_ALUNOS = "alunos";
    public static String TABELA_USUARIO = "usuario";

    public DbHelper(Context context) {
        super(context,NOME_DB,null,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql_alunos = "CREATE TABLE IF NOT EXISTS " + TABELA_ALUNOS
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + " nome TEXT NOT NULL,"
                + "email TEXT NOT NULL );";

        String sql_usuario = "CREATE TABLE IF NOT EXISTS " + TABELA_USUARIO
                + "(_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "nome TEXT NOT NULL,"
                + "email TEXT NOT NULL, "
                + "senha TEXT NOT NULL);";

        try {
            db.execSQL(sql_alunos);
            db.execSQL(sql_usuario);
            Log.i("INFO DB","Sucesso ao criar tabelas");
        }catch (Exception e){
            Log.i("INFO DB","Erro ao criar tabelas "  + e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql_alunos = "DROP TABLE IF EXISTS " + TABELA_ALUNOS + " ;";
        String sql_usuario = "DROP TABLE IF EXISTS " + TABELA_USUARIO + " ;";

        try {
            db.execSQL(sql_alunos);
            db.execSQL(sql_usuario);
            onCreate(db);
            Log.i("INFO DB","Sucesso ao atualizar APP");
        }catch (Exception e){
            Log.i("INFO DB","Erro ao atualizar APP " + e.getMessage());
        }
    }
}
