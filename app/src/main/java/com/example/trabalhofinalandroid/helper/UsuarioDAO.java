package com.example.trabalhofinalandroid.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.trabalhofinalandroid.model.Aluno;
import com.example.trabalhofinalandroid.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {
    private SQLiteDatabase escreve;
    private SQLiteDatabase le;
    private Context context;

    public UsuarioDAO(Context context) {

        this.context = context;
        DbHelper db = new DbHelper(context);
        escreve = db.getWritableDatabase();
        le = db.getReadableDatabase();
    }

    public long salvar(Usuario user) {

        ContentValues cv = new  ContentValues();
        cv.put("nome", user.getNomeAluno());
        cv.put("email", user.getEmailAluno());
        cv.put("senha", user.getSenha());

        try {
            Log.i("teste","Sucesso ao salvar usuário ");
            return escreve.insert(DbHelper.TABELA_USUARIO,null,cv);
        }catch (Exception e){
            Log.i("teste","Erro ao salvar usuário "  + e.getMessage());
            return 0;
        }
    }


    public boolean atualizar(Usuario user) {

            try {
                ContentValues cv = new ContentValues();
                cv.put("nome",user.getNomeAluno());
                cv.put("email",user.getEmailAluno());
                cv.put("senha",user.getSenha());

                String[] args = { user.getId().toString() };

                escreve.update(DbHelper.TABELA_USUARIO,cv,"_id=?",args);
                Log.i("INFO DB","Sucesso ao atualizar Aluno ");
            }catch (Exception e){
                Log.i("INFO DB","Erro ao atualizar Aluno "  + e.getMessage());
                return false;
            }
        return true;
    }

    public boolean deletar(Usuario user) {

            try {
                String[] args = { user.getId().toString() };
                escreve.delete(DbHelper.TABELA_ALUNOS,"_id=?",args);
                Log.i("INFO DB","Sucesso ao atualizar usuário ");
            }catch (Exception e){
                Log.i("INFO DB","Erro ao atualizar usuário "  + e.getMessage());
                return false;
            }

        return true;
    }

    public Usuario listar(Usuario usuario){


            String emailUser = usuario.getEmailAluno();
            String senha = usuario.getSenha();

            try {
                String sql = "";
                Cursor c = null;
                if(usuario.getId() == null){
                   sql = "SELECT * FROM " + DbHelper.TABELA_USUARIO + " WHERE email =? and senha =?;";
                    c = le.rawQuery(sql, new String[] { emailUser, senha });
                }else{
                    sql = "SELECT * FROM " + DbHelper.TABELA_USUARIO + " WHERE _id =?;";
                    c = le.rawQuery(sql, new String[] { usuario.getId().toString() });
                }



                Usuario user = null;
                while (c.moveToNext()){
                     user = new Usuario();

                    Long id = c.getLong(c.getColumnIndex("_id"));
                    String email = c.getString(c.getColumnIndex("email"));
                    String nome = c.getString(c.getColumnIndex("nome"));

                    user.setId(id);
                    user.setNomeAluno(nome);
                    user.setEmailAluno(email);

                    return user;
                }
                c.close();
            }catch (Exception e){
                Log.i("teste",e.getMessage());
                return null;
            }

           return null;
        }
}
