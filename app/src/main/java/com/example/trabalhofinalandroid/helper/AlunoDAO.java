package com.example.trabalhofinalandroid.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.trabalhofinalandroid.model.Aluno;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAO{

    private SQLiteDatabase escreve;
    private SQLiteDatabase le;

    public AlunoDAO(Context context) {

        DbHelper db = new DbHelper(context);
        escreve = db.getWritableDatabase();
        le = db.getReadableDatabase();
    }

    public boolean salvar(Aluno aluno) {

            ContentValues cv = new  ContentValues();
            cv.put("nome", aluno.getNomeAluno());
            cv.put("email", aluno.getEmailAluno());

            try {
                escreve.insert(DbHelper.TABELA_ALUNOS,null,cv);
                Log.i("INFO DB","Sucesso ao salvar Aluno ");
            }catch (Exception e){
                Log.i("INFO DB","Erro ao salvar Aluno "  + e.getMessage());
                return false;
            }

        return true;
    }


    public boolean atualizar(Aluno aluno) {

            try {
                ContentValues cv = new ContentValues();
                cv.put("nome",aluno.getNomeAluno());
                cv.put("email",aluno.getEmailAluno());

                String[] args = { aluno.getId().toString() };

                escreve.update(DbHelper.TABELA_ALUNOS,cv,"_id=?",args);
                Log.i("INFO DB","Sucesso ao atualizar Aluno ");
            }catch (Exception e){
                Log.i("INFO DB","Erro ao atualizar Aluno "  + e.getMessage());
                return false;
            }

        return true;
    }

    public boolean deletar(Aluno aluno) {

            try {
                String[] args = { aluno.getId().toString() };
                escreve.delete(DbHelper.TABELA_ALUNOS,"_id=?",args);
                Log.i("INFO DB","Sucesso ao atualizar Aluno ");
            }catch (Exception e){
                Log.i("INFO DB","Erro ao atualizar Aluno "  + e.getMessage());
                return false;
            }

        return true;
    }

    public List<Aluno> listar() {

            List<Aluno> alunos = new ArrayList<>();

            String sql = "SELECT * FROM " + DbHelper.TABELA_ALUNOS + " ;";
            Cursor c = le.rawQuery(sql,null);
            while (c.moveToNext()){
                Aluno aluno = new Aluno();

                Long id = c.getLong(c.getColumnIndex("_id"));
                String nomeAluno = c.getString(c.getColumnIndex("nome"));
                String emailAluno = c.getString(c.getColumnIndex("email"));

                aluno.setId(id);
                aluno.setNomeAluno(nomeAluno);
                aluno.setEmailAluno(emailAluno);

                alunos.add(aluno);
            }
            c.close();

        return alunos;

    }
}
