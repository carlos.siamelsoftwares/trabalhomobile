package com.example.trabalhofinalandroid.model;

import java.io.Serializable;

public class Aluno implements Serializable {

    private Long id;
    private String nomeAluno;
    private String emailAluno;

    public Aluno(Long id) {
        this.id = id;
    }

    public Aluno() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public String getEmailAluno() {
        return emailAluno;
    }

    public void setEmailAluno(String emailAluno) {
        this.emailAluno = emailAluno;
    }
}
