package com.example.trabalhofinalandroid.model;

public class Usuario extends Aluno {

    private String senha;

    public Usuario(Long id) {
        super(id);
    }

    public Usuario() {
    }



    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
