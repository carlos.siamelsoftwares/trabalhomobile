package com.example.trabalhofinalandroid.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.adapter.AlunoAdapter;
import com.example.trabalhofinalandroid.helper.AlunoDAO;
import com.example.trabalhofinalandroid.helper.Configuracoes;
import com.example.trabalhofinalandroid.helper.PreferenciasUser;
import com.example.trabalhofinalandroid.helper.RecyclerItemClickListener;
import com.example.trabalhofinalandroid.model.Aluno;
import com.example.trabalhofinalandroid.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AlunoAdapter AlunoAdapter;
    private List<Aluno> listaAlunos = new ArrayList<>();
    private Usuario user;
    private Aluno AlunoSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recyclerView = findViewById(R.id.listaAlunos);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Aluno AlunoSelecionado = listaAlunos.get(position);

                                Intent intent = new Intent(MainActivity.this, AdicionarAlunoActivity.class);
                                intent.putExtra("AlunoSelecionado",AlunoSelecionado);

                                startActivity(intent);

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                               AlunoSelecionado = listaAlunos.get(position);

                                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);

                                dialog.setTitle("Confirmar exclusão");
                                dialog.setMessage("Deseja excluir a Aluno " + AlunoSelecionado.getNomeAluno() + " ?");

                                dialog.setPositiveButton("sim", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        AlunoDAO AlunoDAO = new AlunoDAO(getApplicationContext());

                                        if(AlunoDAO.deletar(AlunoSelecionado)){

                                            carregarListaAlunos();
                                            Toast.makeText(getApplicationContext(), "Sucesso ao excluir Aluno!", Toast.LENGTH_LONG).show();

                                        }else{
                                            Toast.makeText(getApplicationContext(), "Erro ao excluir Aluno!", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                });

                                dialog.setNegativeButton("Não",null);

                                dialog.create();
                                dialog.show();

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AdicionarAlunoActivity.class);
                startActivity(intent);
            }
        });

    }

    public void carregarListaAlunos(){
        //Listar Alunos
       AlunoDAO AlunoDAO = new AlunoDAO(getApplicationContext());
       listaAlunos = AlunoDAO.listar();


        //Configura Adapter
        AlunoAdapter = new AlunoAdapter( listaAlunos );

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(AlunoAdapter);
    }

    @Override
    protected void onStart() {
        carregarListaAlunos();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), Configuracoes.class);
                startActivity(intent);
                return true;

            case R.id.lista_servidor:
                startActivity(new Intent(getApplicationContext(), ListaAlunosServidorActivity.class));
                return true;

            case R.id.minhaConta:
                startActivity(new Intent(getApplicationContext(), MinhaContaActivity.class));
                return true;

            case R.id.sair:
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
