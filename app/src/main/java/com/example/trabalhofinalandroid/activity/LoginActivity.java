package com.example.trabalhofinalandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.helper.AlunoDAO;
import com.example.trabalhofinalandroid.helper.PreferenciasUser;
import com.example.trabalhofinalandroid.helper.UsuarioDAO;
import com.example.trabalhofinalandroid.model.Aluno;
import com.example.trabalhofinalandroid.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    EditText senha,emailUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailUser = findViewById(R.id.email);
        senha = findViewById(R.id.senha);

        Intent intent = new Intent(getApplicationContext(),MainActivity.class);

        if(PreferenciasUser.getValuesBoolean(getApplicationContext(),"materConectado")) {
            startActivity(intent);
            finish();
        }




    }


    public void logar(View view){
       UsuarioDAO usuario = new UsuarioDAO(LoginActivity.this);
       final Usuario user = new Usuario();

       if(!emailUser.getText().toString().isEmpty()){
           if(!senha.getText().toString().isEmpty()){
               user.setEmailAluno(emailUser.getText().toString());
               user.setSenha(senha.getText().toString());

               final Usuario user_login = usuario.listar(user);

               if(user_login != null){
                   Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                   final CheckBox checkBox =  findViewById(R.id.checkbox);

                   if(!PreferenciasUser.getValuesBoolean(getApplicationContext(),"materConectado")) {

                       PreferenciasUser.setValuesString(getApplicationContext(), "nameUser", user_login.getNomeAluno());
                       PreferenciasUser.setValuesString(getApplicationContext(), "_id", user_login.getId().toString());

                       if (checkBox.isChecked()) {
                           PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", true);
                       } else {
                           PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                       }
                   }
                   startActivity(intent);
                   finish();
               }else{
                   Toast.makeText(getApplicationContext(), "Usuário não cadastrado!", Toast.LENGTH_LONG).show();
               }
           }else{
               Toast.makeText(getApplicationContext(), "Preencha o campo senha!", Toast.LENGTH_LONG).show();
           }
       }else{
            Toast.makeText(getApplicationContext(), "Preencha o campo e-mail!", Toast.LENGTH_LONG).show();
        }


    }

    public void redirecionarCadastro(View view){
        Intent intent = new Intent(getApplicationContext(),CadastroUsuarioActivity.class);
        startActivity(intent);
        finish();
    }



}
