package com.example.trabalhofinalandroid.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.adapter.AlunoAdapter;
import com.example.trabalhofinalandroid.adapter.AlunoServidorAdapter;
import com.example.trabalhofinalandroid.helper.GetAlunosTask;
import com.example.trabalhofinalandroid.model.Aluno;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListaAlunosServidorActivity extends AppCompatActivity {

    ListView listViewAlunos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos_servidor);

        listViewAlunos = findViewById(R.id.alunosServidor);

        ArrayList<Aluno>  al = (ArrayList<Aluno>) getAlunosServer(getApplicationContext());

        if(al != null) {
            AlunoServidorAdapter adapter = new AlunoServidorAdapter(ListaAlunosServidorActivity.this, R.layout.item_lista_aluno, al);
            listViewAlunos.setAdapter(adapter);
        }


    }


    private static List<Aluno> parserJSON(Context context, String json) throws IOException {
        List<Aluno> alunos = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("alunos");
            JSONArray jsonAlunos = obj.getJSONArray("aluno");


            for (int i=0;i<jsonAlunos.length();i++) {
                Aluno a = new Aluno();
                a.setNomeAluno(jsonAlunos.getJSONObject(i).getString("alunos_nome"));
                a.setEmailAluno(jsonAlunos.getJSONObject(i).getString("alunos_email"));
                alunos.add(a);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return alunos;
    }

    public static List<Aluno> getAlunosServer(Context context) {

        try {
            String json = new GetAlunosTask().execute("http://45.55.53.18/aulamobile/webservices/alunos.json").get();
            return parserJSON(context, json);
        } catch (Exception e) {
            Log.i("erro", e.getMessage());
            return null;
        }
    }
}
