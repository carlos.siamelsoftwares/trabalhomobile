package com.example.trabalhofinalandroid.activity;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.helper.AlunoDAO;
import com.example.trabalhofinalandroid.model.Aluno;

public class AdicionarAlunoActivity extends AppCompatActivity {

    private TextInputEditText nomeAluno,emailAluno;
    private Aluno alunoAtual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_aluno);
        nomeAluno = findViewById(R.id.nomeAluno);
        emailAluno = findViewById(R.id.emailAluno);

        //Recuperar Aluno, caso edição
        alunoAtual = (Aluno) getIntent().getSerializableExtra("AlunoSelecionado");

        //Configurar Aluno na caisa de texto
        if(alunoAtual != null){
            nomeAluno.setText(alunoAtual.getNomeAluno());
            emailAluno.setText(alunoAtual.getEmailAluno());
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_adicionar_aluno,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.itemSalvar:

                String nome = nomeAluno.getText().toString();
                String email = emailAluno.getText().toString();
                AlunoDAO AlunoDAO = new AlunoDAO(getApplicationContext());

                if(alunoAtual != null){
                    if (!nome.isEmpty()) {
                        if(!email.isEmpty()){
                            Aluno Aluno = new Aluno();
                            Aluno.setNomeAluno(nome);
                            Aluno.setEmailAluno(email);
                            Aluno.setId(alunoAtual.getId());

                            //Atualizar banco de dados
                            if(AlunoDAO.atualizar(Aluno)){
                                finish();
                                Toast.makeText(getApplicationContext(), "Sucesso ao atualizar Aluno!", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getApplicationContext(), "Erro ao Atualizar Aluno!", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(this, "Digite o e-mail do Aluno!", Toast.LENGTH_LONG).show();
                        }

                    }
                }else {
                    if (!nome.isEmpty()) {
                        if (!email.isEmpty()) {
                            Aluno Aluno = new Aluno();
                            Aluno.setNomeAluno(nome);
                            Aluno.setEmailAluno(email);

                            if (AlunoDAO.salvar(Aluno)) {
                                finish();
                                Toast.makeText(getApplicationContext(), "Sucesso ao salvar Aluno!", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Erro ao salvar Aluno!", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(this, "Digite o e-mail do Aluno!", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(this, "Digite o nome do Aluno!", Toast.LENGTH_LONG).show();
                    }
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
