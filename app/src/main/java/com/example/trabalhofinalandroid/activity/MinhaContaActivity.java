package com.example.trabalhofinalandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.helper.PreferenciasUser;
import com.example.trabalhofinalandroid.helper.UsuarioDAO;
import com.example.trabalhofinalandroid.model.Usuario;

public class MinhaContaActivity extends AppCompatActivity {

    EditText nomeUser,senha,emailUser,novaSenha;
    String _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minha_conta);

        nomeUser = findViewById(R.id.nome);
        emailUser = findViewById(R.id.email);
        senha = findViewById(R.id.senha);
        _id =   PreferenciasUser.getValuesString(getApplicationContext(), "_id");

        UsuarioDAO userDAO = new UsuarioDAO(getApplicationContext());
        Usuario user = userDAO.listar(new Usuario(Long.parseLong(_id)));

        nomeUser.setText(user.getNomeAluno());
        emailUser.setText(user.getEmailAluno());

    }

    public void atualizar(View view) {


        if(!nomeUser.getText().toString().isEmpty()){
            if(!emailUser.getText().toString().isEmpty()){
                if(!senha.getText().toString().isEmpty()){
                    UsuarioDAO usuario = new UsuarioDAO(MinhaContaActivity.this);
                    Usuario user = new Usuario();


                    user.setNomeAluno(nomeUser.getText().toString());
                    user.setEmailAluno(emailUser.getText().toString());
                    user.setSenha(senha.getText().toString());
                    user.setId(Long.parseLong(_id));

                    boolean salvo = usuario.atualizar(user);

                    if(salvo){
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Usuário atualizado com sucesso", Toast.LENGTH_LONG).show();
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Erro ao salvar usuário!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Digite o senha do usuário!", Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getApplicationContext(), "Digite o e-mail do usuário!", Toast.LENGTH_LONG).show();
            }

        }else{
            Toast.makeText(getApplicationContext(), "Digite o nome do usuário!", Toast.LENGTH_LONG).show();
        }

    }
}
