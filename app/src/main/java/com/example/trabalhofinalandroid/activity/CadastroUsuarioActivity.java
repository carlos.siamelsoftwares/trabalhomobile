package com.example.trabalhofinalandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.helper.PreferenciasUser;
import com.example.trabalhofinalandroid.helper.UsuarioDAO;
import com.example.trabalhofinalandroid.model.Usuario;

import java.util.List;

public class CadastroUsuarioActivity extends AppCompatActivity {

    EditText nomeUser,senha,emailUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);

        nomeUser = findViewById(R.id.nome);
        emailUser = findViewById(R.id.email);
        senha = findViewById(R.id.senha);

    }

    public void cadastrar(View view) {


        if(!nomeUser.getText().toString().isEmpty()){
            if(!emailUser.getText().toString().isEmpty()){
                if(!senha.getText().toString().isEmpty()){
                    UsuarioDAO usuario = new UsuarioDAO(CadastroUsuarioActivity.this);
                    Usuario user = new Usuario();
                    user.setNomeAluno(nomeUser.getText().toString());
                    user.setEmailAluno(emailUser.getText().toString());
                    user.setSenha(senha.getText().toString());

                    long salvo = usuario.salvar(user);

                    if(salvo != 0){
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        final CheckBox checkBox =  findViewById(R.id.checkbox);

                        if(!PreferenciasUser.getValuesBoolean(getApplicationContext(),"materConectado")) {

                            PreferenciasUser.setValuesString(getApplicationContext(), "nameUser", nomeUser.getText().toString());
                            PreferenciasUser.setValuesString(getApplicationContext(), "_id", String.valueOf(salvo));

                            if (checkBox.isChecked()) {
                                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", true);
                            } else {
                                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                            }
                        }
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Erro ao usuário!", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Digite o senha do usuário!", Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(getApplicationContext(), "Digite o e-mail do usuário!", Toast.LENGTH_LONG).show();
            }

        }else{
            Toast.makeText(getApplicationContext(), "Digite o nome do usuário!", Toast.LENGTH_LONG).show();
        }

    }
}
