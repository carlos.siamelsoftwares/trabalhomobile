package com.example.trabalhofinalandroid.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.model.Aluno;

import java.util.List;

public class AlunoAdapter extends RecyclerView.Adapter<AlunoAdapter.MyViewHolder> {

    private List<Aluno> listaAlunos;

    public AlunoAdapter(List<Aluno> lista) {

        this.listaAlunos = lista;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemLista = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_aluno_adapter,viewGroup,false);

        return new  MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Aluno Aluno = listaAlunos.get(i);
        myViewHolder.nome.setText("Nome: " + Aluno.getNomeAluno());
        myViewHolder.email.setText("Email: " + Aluno.getEmailAluno());

    }

    @Override
    public int getItemCount() {
        return this.listaAlunos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nome;
        TextView email;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nome = itemView.findViewById(R.id.nomeAluno);
            email = itemView.findViewById(R.id.emailAluno);

        }
    }

}
