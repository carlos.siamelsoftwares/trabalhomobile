package com.example.trabalhofinalandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trabalhofinalandroid.R;
import com.example.trabalhofinalandroid.model.Aluno;

import java.util.List;

public class AlunoServidorAdapter extends ArrayAdapter<Aluno> {


    private List<Aluno> items;

    public AlunoServidorAdapter(Context context, int textViewResourceId, List<Aluno> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context ctx = getContext();
            LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista_aluno, null);
        }
        Aluno aluno = items.get(position);
        if (aluno != null) {
            ((TextView) v.findViewById(R.id.nome)).setText("Nome: " + aluno.getNomeAluno());
            ((TextView) v.findViewById(R.id.email)).setText("Email: " + aluno.getEmailAluno());
            ((ImageView) v.findViewById(R.id.imagem)).setImageResource(R.drawable.ic_pessoa_cinza_24dp);
        }
        return v;
    }

}
